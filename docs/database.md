## Base de données

## Introduction
Il est toujours utile de documenter un projet et de faire
des schémas avant de rentrer dans la phase de développement. Cela permet
une meilleure compréhension du projet en lui-même et de mieux s'y
retrouver par la suite.