# Collaboration

## Introduction
Toujours dans un effort de simplification et de clarification du projet, notre groupe a décidé
d'utiliser **git** et un dépôt **Gitlab** afin de travailler en toute sérenité. Une telle organisation
permet un versioning des fichiers, afin de revenir facilement à un état précédent.

## Gitlab
Comme expliqué précédemment, le projet est versioné sur **Gitlab**. En ce
qui concerne l'organisation, nous avons tout simplement notre projet
*gestion-stages* dans un groupe *bts-sio-camille-see*, encore une fois pour
séparer les choses.

## Git Flow
En plus d'utiliser **git**, nous utilisons **Git Flow** de manière a travailler
plus efficacement. Cet outil nous permet aussi une simplification des
commandes **git**.