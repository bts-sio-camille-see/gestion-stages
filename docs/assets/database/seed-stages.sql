-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  lun. 29 juin 2020 à 08:48
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion_stages`
--

--
-- Déchargement des données de la table `organisation`
--

INSERT INTO `organisation` (`nom`, `telephone`, `email`, `site`, `secteur_activite`,`adresse_rue`, `code_postal`, `ville`, `numero_SIRET`, `code_APE`) VALUES
('Ten Twenty Four sarl', '+-35 22 02 11 10 24', '', '', '', '1 place de l\'hôtel de ville', '', 'Luxembourg', '', ''),
('Mairie d\'Illzach', '03 89 62 53 00', '', '', '', '9 place de la république', '', 'Illzach', '', ''),
('SARL DEG Le Pastoral', '03 88 74 84 61', '', '', '', '24 rue de général De Gaulle', '', 'Huttenheim', '', ''),
('CER SNCF Alsace', '03 88 75 38 90', '','', '', '8 rue de Koenigshoffen', '', 'Strasbourg', '', ''),
('DDT du Haut-Rhin', '09 89 24 83 01', '', '', '', 'Cité administrative, tour, 3 rue fleischhauer', '', 'Colmar', '', ''),
('ADIS - AGIPI', '09 90 23 90 00', '', '', '', '12 avenue Pierre Mendès France', '', 'Schiltigheim', '', ''),
('PC Clic Urgence', '09 89 89 41 38', '', '', '', '32 rue de Mulhouse', '', 'Saint-Louis', '', ''),
('Concept Office Online', '03 89 42 12 77', '', '', '', '4 rue Poincaré', '', 'Mulhouse', '', ''),
('L.I.S.', '09 72 11 78 07', '', '', '', '9 rue d\'Enschingen', '', 'Spechbach', '', ''),
('Vialis', '03 69 99 61 36', '', '', '', '10 rue des bonnes gens', '', 'Colmar', '', ''),
('Groupe Larger Communication', '09 89 66 29 52', '', '', '', '23C rue de la Hardt', '', 'Sausheim', '', ''),
('DDT du Haut-Rhin', '09 89 24 83 01', '', '', '', 'Cité administrative, tour, 3 rue fleischhauer', '', 'Colmar', '', ''),
('Centre hospitalier des vals d\'Ardèche', '04 75 20 20 04', '', '', '', 'Avenue Pasteur', '', 'Privas', '', ''),
('Histoire du temps', '03 89 26 94 99', '', '', '', '11 rue des fabriques', '', 'Husseren-Wesserlin', '', ''),
('Orgazone', '03 89 23 02 46', '', '', '', '11 place du capitaine Dreyfus', '', 'Colmar', '', ''),
('Greta Colmar', '03 89 22 92 22', '', '', '', '74 rue du Loguelbach (lycée Blaise Pascal)', '', 'Colmar', '', ''),
('Gravinda SAS', '03 67 11 29 38', '', '', '', '61 rue Albert Schoelcher', '', 'Mulhouse', '', ''),
('ONF', '03 89 22 96 10', '', '', '', '22 rue de Herrlisheim', '', 'Colmar', '', ''),
('Greta Colmar', '03 89 22 92 22', '', '', '', '74 rue du Loguelbach (lycée Blaise Pascal)', '', 'Colmar', '', ''),
('Cocktalis International', '03 89 89 42 31', '', '', '', '6 rue de Nancy', '', 'Hésingue', '', ''),
('ONF', '03 89 22 96 10', '', '', '', '22 rue de Herrlisheim', '', 'Colmar', '', ''),
('Centre Leclerc', '03 89 74 24 34', '', '', '', 'Rue de Guebwiller', '', 'Issenheim', '', ''),
('ONF', '03 89 22 96 10', '', '', '', '22 rue de Herrlisheim', '', 'Colmar', '', ''),
('Fr Sauter AG', '', '', '', '', 'Im Surinam 55, CH-4016 BASEL', '', 'Bâle', '', ''),
('Autoentreprise Thibault Weber', '07 86 49 01 87', '', '', '', '25 rue de la station', '', 'Aspach-Le-Bas', '', ''),
('DDT du Haut-Rhin', '09 89 24 83 01', '', '', '', 'Cité administrative, tour, 3 rue fleischhauer', '', 'Colmar', '', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
