#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Organisation
#------------------------------------------------------------

CREATE TABLE Organisation
(
        id               INT  AUTO_INCREMENT NOT NULL,
        nom              VARCHAR(255) NOT NULL,
        telephone        VARCHAR(16) NOT NULL,
        email            VARCHAR(255) NOT NULL,
        site             VARCHAR(255) NOT NULL,
        secteur_activite VARCHAR(255) NOT NULL,
        adresse_rue      VARCHAR(255) NOT NULL,
        code_postal      INT NOT NULL,
        ville            VARCHAR(255) NOT NULL,
        numero_SIRET     VARCHAR(255) NOT NULL,
        code_APE         VARCHAR(5) NOT NULL,
        CONSTRAINT Organisation_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: MaitreStage
#------------------------------------------------------------

CREATE TABLE MaitreStage
(
        id              INT  AUTO_INCREMENT NOT NULL,
        nom             VARCHAR(255) NOT NULL,
        prenom          VARCHAR(255) NOT NULL,
        email           VARCHAR(255) NOT NULL,
        telephone       VARCHAR(16) NOT NULL,
        id_Organisation INT NOT NULL,
        CONSTRAINT MaitreStage_PK PRIMARY KEY (id),
        CONSTRAINT MaitreStage_Organisation_FK FOREIGN KEY (id_Organisation) REFERENCES Organisation(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Promotion
#------------------------------------------------------------

CREATE TABLE Promotion
(
        id          INT AUTO_INCREMENT  NOT NULL,
        annee_debut INT NOT NULL,
        annee_fin   INT NOT NULL,
        commentaire MEDIUMTEXT NOT NULL,
	CONSTRAINT Promotion_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: PeriodeStage
#------------------------------------------------------------

CREATE TABLE PeriodeStage
(
        id           INT  AUTO_INCREMENT NOT NULL,
        DATE_debut   DATE NOT NULL,
        DATE_fin     DATE NOT NULL,
        commenaire   MEDIUMTEXT NOT NULL,
        annee        INT NOT NULL,
        id_Promotion INT NOT NULL,
	CONSTRAINT PeriodeStage_PK PRIMARY KEY (id),
        CONSTRAINT PeriodeStage_Promotion_FK FOREIGN KEY (id_Promotion) REFERENCES Promotion(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur
(
        id              INT AUTO_INCREMENT NOT NULL,
        nom_utilisateur VARCHAR(255) NOT NULL,
        mot_de_passe    VARCHAR(255) NOT NULL,
        role            CHAR NOT NULL,
        remember_token  VARCHAR(255),
	CONSTRAINT Utilisateur_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Etudiant
#------------------------------------------------------------

CREATE TABLE Etudiant
(
        id             INT AUTO_INCREMENT NOT NULL,
        nom            VARCHAR(50) NOT NULL,
        prenom         VARCHAR(50) NOT NULL,
        telephone      VARCHAR(16) NOT NULL,
        email          VARCHAR(50) NOT NULL,
        specialite     VARCHAR(50) NOT NULL,
        annee          INT NOT NULL,
        id_Promotion   INT NOT NULL,
        id_Utilisateur INT NOT NULL,
	CONSTRAINT Etudiant_PK PRIMARY KEY (id),
	CONSTRAINT Etudiant_Promotion_FK FOREIGN KEY (id_Promotion) REFERENCES Promotion(id),
	CONSTRAINT Etudiant_Utilisateur0_FK FOREIGN KEY (id_Utilisateur) REFERENCES Utilisateur(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Stage
#------------------------------------------------------------

CREATE TABLE Stage
(
        id                        INT AUTO_INCREMENT NOT NULL,
        date_debut                DATE NOT NULL,
        date_fin                  DATE NOT NULL,
        signature                 VARCHAR(255) NOT NULL,
        avis                      MEDIUMTEXT NOT NULL,
        annee                     INT NOT NULL,
        reponse                   BOOL NOT NULL,
        etat_avancement_signature VARCHAR(255) NOT NULL,
        id_Etudiant               INT NOT NULL,
        id_Organisation           INT NOT NULL,
        id_PeriodeStage           INT NOT NULL,
        id_MaitreStage            INT NOT NULL,
	CONSTRAINT Stage_PK PRIMARY KEY (id),
	CONSTRAINT Stage_Etudiant_FK FOREIGN KEY (id_Etudiant) REFERENCES Etudiant(id),
	CONSTRAINT Stage_Organisation0_FK FOREIGN KEY (id_Organisation) REFERENCES Organisation(id),
	CONSTRAINT Stage_PeriodeStage1_FK FOREIGN KEY (id_PeriodeStage) REFERENCES PeriodeStage(id),
	CONSTRAINT Stage_MaitreStage2_FK FOREIGN KEY (id_MaitreStage) REFERENCES MaitreStage(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Professeur
#------------------------------------------------------------

CREATE TABLE Professeur
(
        id             INT AUTO_INCREMENT NOT NULL,
        nom            VARCHAR(255) NOT NULL,
        prenom         VARCHAR(255) NOT NULL,
        email          VARCHAR(255) NOT NULL,
        id_Utilisateur INT NOT NULL,
	CONSTRAINT Professeur_PK PRIMARY KEY (id),
        CONSTRAINT Professeur_Utilisateur_FK FOREIGN KEY (id_Utilisateur) REFERENCES Utilisateur(id)
)ENGINE=InnoDB;
