<?php
// Initialisation de l'auto loader
require_once 'inc/bootstrap.php';

// Connexion à la base de données
$db = App::getDatabase();

$auth = App::getAuth();
$auth->connectFromCookie($db);

if ($auth->user())
{
	App::redirect('dashboard.php');
}

// Vérification du formulaire
if (!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password']))
{
	$user = $auth->login($db, $_POST['username'], $_POST['password'], isset($_POST['remember']));
	$session = Session::getInstance();
	if ($user)
	{
		$session->setFlash('success', "Vous êtes maintenant connecté");
		App::redirect('dashboard.php');
	} else
	{
		$session->setFlash('danger', "Une erreur est survenue.");

    }
}
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Connexion</title>
    </head>
    <body class="text-center">
	<?php if (Session::getInstance()->hasFlashes()): ?>
		<?php foreach (Session::getInstance()->getFlashes() as $type => $message): ?>
			<div class="alert alert-<?= $type ?>">
				<?= $message; ?>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

        <form class="form-signin" method="post">
            <h1 class="h3 mb-3 font-weight-normal">Connexion</h1>
            <label for="inputUsername" class="sr-only">Nom d'utilisateur :</label>
            <input type="text" id="inputUsername" name="username" class="form-control" placeholder="Nom d'utilisateur" required autofocus>
            <label for="inputPassword" class="sr-only">Mot de passe :</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Mot de passe" required>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember"> Se souvenir de moi
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Se connecter</button>
        </form>
    </body>
</html>