<?php


class Validator
{
    private $data;
    private $errors = [];

    public function __construct($data)
    {
        $this->data = $data;
    }

    private function getField($field)
    {
        if (!isset($this->data[$field]))
        {
            return null;
        }
        return $this->data[$field];
    }

    public function isAlphanumeric($field, $errorMessage)
    {
        if (!preg_match('/^[a-zA-Z0-9_]+$/', $this->getField($field)))
        {
            $this->errors[$field] = $errorMessage;
        }
    }

    public function isUnique($field, $db, $table, $errorMessage)
    {
        $record = $db->query("SELECT id FROM $table WHERE $field = ?", [$this->getField($field)])->fetch();
        if ($record)
        {
            $this->errors[$field] = $errorMessage;
        }
    }

    public function isEmail($field, $errorMessage)
    {
        if (!filter_var($this->getField($field), FILTER_VALIDATE_EMAIL))
        {
            $this->errors[$field] = $errorMessage;
        }
    }

    public function isConfirmed($field, $errorMessage = "")
    {
        if (empty($this->getField($field) || $this->getField($field) != $this->getField($field . '_confirmed')))
        {
            $this->errors[$field] = $errorMessage;
        }
    }

    public function isValid()
    {
        return empty($this->errors);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}