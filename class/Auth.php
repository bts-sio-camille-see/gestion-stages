<?php


class Auth
{
    private $options = [];
    private $session;

    public function __construct($session, $options = [])
    {
        $this->options = array_merge($this->options, $options);
        $this->session = $session;
    }

    public function hashPassword($password)
    {
        return password_hash($password, PASSWORD_ARGON2I);
    }

    public function register($db, $username, $password, $role)
    {
        $password = $this->hashPassword($password);
        $db->query("INSERT INTO utilisateur SET nom_utilisateur = ?, mot_de_passe = ?, role = ?",
        [
            $username,
            $password,
            $role
        ]);
    }

    public function restrict()
    {
        if (!$this->session->read('auth'))
        {
            $this->session->setFlash('danger', $this->options['restriction_message']);
            header("Location: login.php");
            exit();
        }
    }

    public function user()
    {
        if ($this->session->read('auth'))
        {
            return false;
        }
        return $this->session->read('auth');
    }

    public function connect($user)
    {
        $this->session->write('auth', $user);
    }

    public function connectFromCookie($db)
    {
        if (isset($_COOKIE['remember']) && !$this->user())
        {
            $remember_token = $_COOKIE['remember'];
            $parts = explode("==", $remember_token);
            $user_id = $parts[0];

            $user = $db->query("SELECT * FROM utilisateur WHERE id = ?", [$user_id])->fetch();
            if ($user)
            {
                $expected = $user_id . '==' . $user->remember_token . sha1($user_id, 'gestionstages');
                if ($expected == $remember_token)
                {
                    $this->connect($user);
                    setcookie('remember', $remember_token, time() * 60 * 60 * 24 * 7);
                } else
                {
                    setcookie('remember', null, -1);
                }
            } else
            {
                setcookie('remember', null, -1);
            }
         }
    }

    public function login($db, $username, $password, $remember = false)
    {
        $user = $db->query("SELECT * FROM utilisateur WHERE nom_utilisateur = :nom_utilisateur", ['nom_utilisateur' => $username])->fetch();
        if(password_verify($password, $user->mot_de_passe))
        {
            $this->connect($user);
            if ($remember)
            {
                $this->remember($db, $user->id);
            }
            return $user;
        } else
        {
            return false;
        }
    }

    public function remember($db, $user_id)
    {
        $remember_token = Str::random(250);
        $db->query('UPDATE utilisateur SET remember_token = ? WHERE id = ?', [$remember_token, $user_id]);
        setcookie('remember', $user_id . "==" . $remember_token . sha1($user_id . 'gestionstage'), time() * 60 * 60 * 24 * 7);
    }

    public function logout()
    {
        setcookie('remember', null, -1);
        $this->session->delete('auth');
    }
}