<?php


class App
{
    static $db = null;

    static function getDatabase()
    {
        if (!self::$db)
        {
            self::$db = new Database('root', '', 'gestion_stages');
            return self::$db;
        }
    }

    static function getAuth()
    {
        return new Auth(Session::getInstance(), ["restriction_message" => "Vous n'avez pas le droit d'accéder à cette page."]);
    }

    static function redirect($path)
    {
        header("Location: $path");
        exit();
    }
}