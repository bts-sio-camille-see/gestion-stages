<?php
require 'inc/bootstrap.php';

App::getAuth()->restrict();
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Gestion Stages</title>
    </head>
    <body>
    <?php if (Session::getInstance()->hasFlashes()): ?>
        <?php foreach (Session::getInstance()->getFlashes() as $type => $message): ?>
            <div class="alert alert-<?= $type ?>">
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    </body>
</html>
